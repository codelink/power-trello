var webview = document.getElementById('main');
var extID = 'ppibnbapebejhnmplokgfhijhfdchhhc'; // catcher links extension
var appID = 'gkcknpgdmiigoagkcoglklgaagnpojed'; // this app

// set some css on trello.com
webview.addEventListener('loadcommit', function(e) {
    if (e.isTopLevel) {
        webview.insertCSS({
            code: '.header-btn.header-boards, .promo-nav { margin-left: 38px !important; }',
            runAt: 'document_start'
        });
    }
});

// send new-window-links to bronser
webview.addEventListener('newwindow', function(e) {
    e.stopImmediatePropagation();
    window.open(e.targetUrl);
});

// hotkeys
window.addEventListener('keydown', function(e) {
    // Ctrl+R or F5
    if (e.ctrlKey && e.keyCode == 82 || e.keyCode == 115) {
        webview.reload();
    }

    // F11
    if (e.keyCode == 122) {
        if (chrome.app.window.current().isFullscreen()) {
            chrome.app.window.current().restore();
        } else {
            chrome.app.window.current().fullscreen();
        }
    }
});

// fix webview lose focus
window.addEventListener('focus', function(e) {
    webview.focus();
});

// open cached links
chrome.runtime.onMessage.addListener(function(request, sender) {
    if (sender.id == appID) {
        webview.src = request;
    }
});

var wv = document.querySelector('webview');
var rebooted = false;
// loadcommit instead of contentload:
wv.addEventListener('loadcommit', function(e) {
    this.addContentScripts([
     {
        name: 'rule',
            matches: ['https://trello.com/b/*'],
            css: { files: ['agilescrum/agilescrumtrello.css', 'trelabels/trelabels.css', 'trellists/trellists.css'] },
            js: { files: ['jquery.min.js', 'jquery.waituntilexists.min.js', 'beautify/angular.min.js', 'agilescrum/agilescrumtrello.min.js', 'trelabels/trelabels.js', 'trellists/trellists.js', 'custom.js'] },
        run_at: 'document_end'
    }
    ]);
    if (!rebooted && wv.src.indexOf("https://trello.com/b/") > -1) {
        wv.reload();
        rebooted = true;
    }
});

var AST = function(e) {
    var t = "Agile SCRUM for Trello boards",
        n = "1.4.1",
        r = /\([0-9\.]{1,3}\/[0-9\.]{1,3}\)/,
        i = /\([0-9\.]{1,3}\//,
        s = /\/[0-9\.]{1,3}\)/,
        o = /\([0-9\.]{1,5}\)/,
        u = /[0-9\.]+/,
        a = /\[([a-zA-Z0-9 \_\-\.]*)\]/g,
        f = />\*{3} .+ \*{3}$/i,
        l = 1,
        c = !1,
        h = "",
        p = !1,
        d = !1,
        v = 1500,
        m = !1,
        points = 0,
        totalPointsInList = 0,
        b = !1,
        pointsAfterTitle = 0,
        totalPointsAfterTitle = 0,
        S = !1,
        x = 0,
        T = 0,
        N = !1,
        C = !1;
    e.init = function() {
        console.info(t + " v" + n + " started");
        k();
        A()
    };
    var k = function() {
        $(".header-user").prepend('<a class="header-btn js-open-agilescrum-menu" href="#" title="' + t + ' Manual"><span class="header-btn-icon icon-lg icon-add light"></span></a>');
        $("body").on("mouseup keyup", function() {
            L()
        })
    }, L = function() {
        d || (p = setTimeout(function() {
            A();
            d = !1
        }, v));
        d = !0
    }, A = function() {
        c = $("body").css("background-color") && !$("body").hasClass("body-custom-board-background") ? $("body").css("background-color") : "rgb(55, 158, 90)";
        O();
        $("#board").find(".list-cards").each(function() {
            m = $(this);
            m.find(".list-card").each(function() {
                S = $(this);
                if (M(S)) return -2;
                if (!$(S).find(".list-card-title").html().match(/<small /g)) {
                    N = $(S).find(".list-card-title").html();
                    S.data("original", N)
                } else {
                    N = S.data("original");
                    $(S).find(".list-card-title").html(N)
                } if (_(N)) {
                    totalPointsAfterTitle > 0 && D();
                    b = S;
                    b[0].innerHTML = b[0].innerHTML.replace(/( ?\*\*\* ?)/g, "");
                    b.addClass("scrum-card-header")
                } else if (N.match(r)) {
                    C = N.match(r)[0];
                    x = P(C.match(i)[0]);
                    T = P(C.match(s)[0])
                } else N.match(o) && (T = P(N.match(o)[0]));
                x == T ? h = " perfect" : x > T ? h = " overrun" : h = "";
                S[0].innerHTML = S[0].innerHTML.replace(o, B);
                S[0].innerHTML = S[0].innerHTML.replace(r, B);
                S[0].innerHTML = S[0].innerHTML.replace(a, H);
                if (T > 0) {
                    S.prepend('<div class="scrum-card-progress' + h + '" style="background-color:' + c + ";width:" + x / T * 100 + '%"></div>');
                    $(S[0]).css("font-size", (T < 8 ? 90 + 5 * T : 130) + "%").css("line-height", "1.2em")
                }
                points += x;
                totalPointsInList += T;
                if (b) {
                    pointsAfterTitle += x;
                    totalPointsAfterTitle += T
                }
                S = !1;
                N = C = "";
                T = x = 0
            });
            totalPointsAfterTitle > 0 && D();
            if (totalPointsInList > 0) {
                m.parent().prepend('<small class="scrum-list-total' + h + '"><span class="scrum-light">' + points.toFixed(l) + "/</span>" + totalPointsInList.toFixed(l) + "</small>");
            }
            totalPointsInList > 0 && m.parent(".list").prepend('<div class="scrum-list-progress"  style="background-color:' + c + ";width:" + points / totalPointsInList * 100 + '%"></div>');
            m = !1;
            points = totalPointsInList = 0
        })
    }, O = function() {
        $(".scrum-list-total,.scrum-list-progress,.scrum-card-progress").remove()
    }, M = function() {
        return S.hasClass("js-composer")
    }, _ = function(e) {
        return e.match(f)
    }, D = function() {
        $(b.find(".list-card-title")[0]).prepend('<small class="scrum-card-points' + (pointsAfterTitle > totalPointsAfterTitle ? " overrun" : "") + '"><span class="scrum-light">' + pointsAfterTitle.toFixed(l) + "/</span>" + totalPointsAfterTitle.toFixed(l) + "</small>");
        pointsAfterTitle = totalPointsAfterTitle = 0
    }, P = function(e) {
        return parseFloat(e.match(u)[0])
    }, H = function(e) {
        return '<small class="scrum-card-project" style="background:' + j(e) + '">' + e.replace(/\[|\]/g, "").toUpperCase() + "</small>"
    }, B = function(e) {
        return '<small class="scrum-card-points' + h + '">' + e.replace(/\(|\)/g, "").toUpperCase() + "</small>"
    }, j = function(e) {
        var t = 0,
            n = 0,
            r = e.length;
        e = e.toLowerCase();
        for (; n < r; n++) t += e.charAt(n).charCodeAt() * 900;
        return "hsla(" + t % 256 + ",50%,60%,1)"
    };

    return e
}(AST || {});
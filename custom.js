$(function() {
    $('body').on('click', '.js-open-agilescrum-menu', function() {
        document.getElementById('agile-parent');
        if (document.getElementById('agile-parent') == null) {
            var div = document.createElement('div');
            div.id = 'agile-parent';
            div.innerHTML = '<div id="agile-scrum-menu" class="pop-over is-shown">'
                + '<div class="pop-over-header js-pop-over-header">'
                + '    <span class="pop-over-header-title js-fill-pop-over-title">Power Trello</span>'
                + '</div>'
                + '<div class="pop-over-content u-fancy-scrollbar js-fill-pop-over-content js-tab-parent" style="">'
                + '    <div>'
                + '    <ul class="pop-over-list">'
                + '    <li>'
                + '    <div class="js-new-board">'
                + '    <div style="color: #444; cursor: pointer; display: block; font-weight: 700; padding: 6px 10px; position: relative; margin: 0 -10px; text-align: center;">Story Points'
                + '</div>'
                + '<span class="sub-name"> Add Story Points to a card by typing the number in parenthesis:'
                + '</span>'
                + '<div><span>'
                + '<b>(3)</b> Design new homepage header'
                + '</span></div>'
                + '</div>'
                + '</li>'
                + '<li>'
                + '<div class="js-new-board">'
                + '    <div style="color: #444; cursor: pointer; display: block; font-weight: 700; padding: 6px 10px; position: relative; margin: 0 -10px; text-align: center;">Time Spent'
                + '</div>'
                + '<span class="sub-name"> Track time spent on each card by typing a portion between parenthesis:'
                + '    </span>'
                + '<div><span>'
                + '<b>(1/3)</b> Design new homepage header'
                + '</span></div>'
                + '</div>'
                + '</li>'
                + '<li>'
                + '<div class="js-new-board">'
                + '    <div style="color: #444; cursor: pointer; display: block; font-weight: 700; padding: 6px 10px; position: relative; margin: 0 -10px; text-align: center;">Project'
                + '    </div>'
                + '    <span class="sub-name"> Assign a card to a Project by writing it in square braquets:'
                + '    </span>'
                + '<div><span>'
                + '<b>[dev]</b> Implement Ads in footer'
                + '</span></div>'
                + '</div>'
                + '</li>'
                + '<li>'
                + '<div class="js-new-board">'
                + '    <div style="color: #444; cursor: pointer; display: block; font-weight: 700; padding: 6px 10px; position: relative; margin: 0 -10px; text-align: center;">Header Separator'
                + '</div>'
                + '<span class="sub-name"> Create a Header Separator by creating a new card with three asterisks at the start and end:'
                + '    </span>'
                + '<div><span>'
                + '<b>*** Sprint 3 ***</b>'
                + '</span></div>'
                + '</div>'
                + '</li>'
                + '<li>'
                + '<div class="js-new-board">'
                + '    <div style="color: #444; cursor: pointer; display: block; font-weight: 700; padding: 6px 10px; position: relative; margin: 0 -10px; text-align: center;">Refresh: Ctrl - R'
                + '</div>'
                + '</div>'
                + '</div>'
                + '</li>'
                + '</ul>'
                + '</div>'
                + '</div>'
                + '</div>';
            document.getElementsByTagName("body")[0].appendChild(div);

            var menu = $('#agile-scrum-menu');
            menu.attr('style', "top: 41px; right: 5px;");
        } else {
            document.getElementsByTagName('body')[0].removeChild(document.getElementById('agile-parent'));
        }
        //menu.toggle();
    });

    /*var div = document.createElement('div');
     div.innerHTML = '<a class="header-btn beautifyshow" href="#" title="Agile SCRUM for Trello boards Settings"><span class="header-btn-icon icon-lg icon-edit light"></span></a>';
     $('.header-search').parent().append(div.children[0]);*/
});
